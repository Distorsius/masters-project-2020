Robin Pitteloud 2020 Master's Project
University of Lausanne

# Oberon, litterature tutor.

## Introduction

Oberon is a project that began it's early development in summer 2019. The initial goal of the project was to create a quizz system that allowed extraction of structured data, that define the [semantic web](https://en.wikipedia.org/wiki/Semantic_Web), and exploited it to generate questions automatically.

To this end, this unnamed original version exploited the very rich database of [DBpedia](https://wiki.dbpedia.org/), a semi automatic conversion of Wikipedia into structured data.

In order to restrict it's scope, the domain of litterature, specifically books and their authors, was selected as a focus of the quizz. Allowing me to work with a small-ish subset of datapoints.

Indeed, these data mostly in the form of "subject, predicate, object" [triples](https://en.wikipedia.org/wiki/Resource_Description_Framework) of data, that define both an [ontology](https://en.wikipedia.org/wiki/Web_Ontology_Language), meaning the concepts, categories and relationship that define individuals, aswell as the individuals and their caracteristic themselves.

For instance, if one were to describe the book "Dune" by Frank Herbert in the specification demanded by the semantic web, one would create a file that would look somewhat like this:

> \#Prefix o https://myontology.owl/
>
> \#Prefix r https://myontology.owl/resources/
>
> r:Dune_(Novel) o:author r:Frank_Herbert
>
> r:Dune_(Novel) o:date 1965
>
> r:Dune_(Novel) o:country r:United_States
>
> etc, etc.

These triples inform us of three things about Dune: it's author, it's parution date, and it's country of origin. 

As you can see, the first element of each line is r:Dune_(Novel), this is an URI (or Uniform Resource Identifier), with a prefix r, defined beforehand. Thus, this URI in it's full form is https://myontology.owl/resources/Dune_(Novel). This means that this triple will be about this specific resource (or individual), as the first element of a triple is the subject.

The second element of each triple is the predicate, it defines what category the last element belongs to in relation to the first. In this example, we can see that r:Frank_Herbert is the o:author of r:Dune_(Novel). In layman's terms, it means that the relationship between Frank Herbert and Dune is that the former is the author of the latter.

Upper ontologies, meaning the categories themselves (o:author, o:date, o:country) are defined in the same way, in the form of relationships between each other, forming trees of meaning and specifications that range from what is a "thing" to what is a page in a book.

The original idea for Oberon was to create an interfacing layer between these triples stores and humans, in the form of a game, allowing ease of access to an extremely rich source of information in an (hopefully) fun way.

One of the greatest limitations of this initial version was how random it was, as nothing controlled the flow of the game, such as the difficulty, nature, and subjects of questions. The sheer chaos of it all rendered this prototype quite boring to play, and inefficient as a learning tool.

In order to overcome these limitations, I decided, as a master's project, to implement a [Reinforcement Learning](https://en.wikipedia.org/wiki/Reinforcement_learning) algorithm that would learn about the dataset and the players, in order to maximize learning.

These kinds of programs are called ITS, for [Intelligent Tutoring Systems](https://en.wikipedia.org/wiki/Intelligent_tutoring_system), and are nearly as old as computing itself. In their early days, ITS were already focused on quizzes. Earliest examples include rewired typewriters that could be programmed with punchcards to understand keypresses as correct or incorrect answers to questions printed on a sheet, allowing them to provide instantaneous feedback to the student. As technology evolved, this very simple approach vas overturned by more complex designs, capable of providing guidance in the learning material according to the student learning profile. The advent of machine learning proved a turning point of ITS, allowing near-human ability to teach in the most advanced forms of artificial tutors.

Oberon is somewhat simple in it's approach, because it uses advanced technologies of Reinforcement Learning in order to achieve the same thing these old mechanical typewriters did.

Reinforcement learning is a very complex subject that would take paragraphs upon paragraphs to fully explain, but the gist of it is that RL algorithms aim at one thing and one thing only: maximize their reward function. The reward function is defined by the environment, the problem, or the game the AI is trying to solve. For instance, a tic-tac-toe game would reward a player for winning (or playing a winning move) and punish him for loosing (or playing a loosing move). With that, the RL algorithm will then explore the game by playing it, at first at random, in order to establish the rewards and punishments associated with playing a certain move in a certain state of the game.

Once that is done, the algorithm will decide upon a policy that the AI will follow. This policy, or ruleset or behaviour, describe the set of moves that the algorithm must play in order to gain the maximum amount of reward, thus, in our tic-tac-toe example, win the game.

In the case of Oberon, the algorithm has for environment the questions it can ask and how it can ask them, it sees the questions it already asked and if the player answered them correctly or not. The reward function was designed in a somewhat naive way, assuming answering correctly to a question is preferable to answering it incorrectly, and that a measure of repetition is favorable to memorisation.

In this very early state, the policy followed by Oberon is almost completely random, but as more questions are asked, and more experienced gained, the decisions of the system will be more and more purposeful, and meaningful.

So dont hesitate playing, even if you're not an expert, your input is greatly appreciated, and even one game helps Oberon learn how to do it's job better every time!

### How the game works

If you dont understand what you're supposed to do when you play, here is a quick explanation of the overall rules.
Because it's a game after all, the goal is to maximize your own score. The game notes you on two aspect: every correct answer grants you one point and every wrong answer takes a point away. Alternatively, the game also keeps track of what type of questions you've answered, and notes you on your "metascore" for each good answer.

A game last 10 questions. As such, the maximum possible score is 10, and the minimum score is -10.
Dont be discouraged if you dont score well, while the database Oberon pull it's questions is absurdly vast, the goal is still to make you learn. And no one will ever notice a little cheating now, will they?

The anatomy of a question is as such:
>Who was the author of Dune? (Possible hint)
>1. JRR Tolkien
>2. Stephen Hawkin
>3. CS Lewis
>4. Frank Herbert

All questions wont have a hint, and in the beggining, most hints will be completely meaningless, as Oberon learns what kind of hints are most efficient.
At this point, the game will ask for your answer. You're free to freehand your answer and type it in full if you like, but you can also simply enter the number of the answer.

As said before, 10 questions will be asked, and you'll be given your final scoring at the end.
Every question helps Oberon to learn, but the experience wont be uploaded to the long term distant memory unless you finish the game.
You're free to give up midway through, but it would be very nice if you could finish as much of your games as you can.

Because Oberon runs inside a Python prompt, you can type exit() to leave the game and return to a cmd prompt.
You're also returned to the terminal at the end of the game.

## Installation and usage

Installation requires Python 3.6+

If you're unfamilliar with git, you may download a zip version of this folder to your local machine from this very page.

Right next to the blue clone button at the top of the page, there is a download button. Click it, choose the compression format (zip should be your default), and downloading will begin.

Optionally, you might want to create a virutal environment for python to run in, if you already use python for anything else, you can follow this [tutorial](https://docs.python.org/3/library/venv.html) to achieve this.

Once you're virtual environment is set up, or not if you choose not to use one, run the command 
>`python -m pip install -r requirements.txt`
in order to install necessary packages.

In order to initiate a minimal installation of the program, please run qasystem.py before anything else.
A complete installation of the system is not possible as of this alpha version.

After installation is complete, you may begin playing the game by running the algorithm of your choice.

1. random_algo.py is a minimalistic algorithm that asks question at pure random, without any AI. NOTA BENE: this algorithm never calls for the augmentation of your local database, and as such, you'll see the same questions a lot if you only play this version.
2. q_learning.py is a linear-algebra heavy approach that calculates what is known as a Q value, that represents how good the agent is doing, based on a state-action table that is generated and updated on the fly. It is the most reactive and one of the most powerful forms of RL to date.

## What to do to help

The game collects it's own feedback in the form of experience for the agent, but your direct feedback would be hugely appreciated.
You can submit it with this very quick survey : [https://www.surveymonkey.com/r/6DX63SX](https://www.surveymonkey.com/r/6DX63SX).
If you're feeling inspired, you may also write at my email adress distorsius@gmail.com
But I cannot guarantee I'll read it, let alone respond to it.


Thanks a lot for playing!

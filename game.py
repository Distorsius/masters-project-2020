"""
Masters project 2020-2021 - Oberon
"""

import os, re, string, sys
import random
import traceback
from pprint import pprint
import json
from bson import json_util
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from qasystem import QAsystem
# import streamlit as st

dirname = os.path.dirname(__file__)



class QA:
    """
    This class contains the method and functions that pertains to
    the actual gameplay. It describes the functions that allow to create
    and ask questions that are derived from the databases created in QAsystem.
    """

    def __init__(self):

        # score is the normally printed score that the user see
        self.score = 0
        self.question_number = 0

        # This block instanciate and loads the available books of the local database into the masterdict attribute of system.
        # it also purges obviously corrupted entries in the dictionnary
        self.system = QAsystem()
        list_of_books = self.system.obtainAllTitles()
        try:
            del self.system[None]
            del self.system[""]
        except:
            pass


        for book in list_of_books:
            self.system.describeBook(book)

        # In order to prevent untimely crashes, we also declare _previous_question and give it a default value.
        self._previous_question = "Dune"

        # the metascore of each kind of metadata is linked to the availability of completion questions for each kind of metadata for the user
        self.metascore = {"title": 0, "genres": 0, "author": 0, "pubDate": 0, "pubLoc": 0, "publisher": 0}
            

    def askQuestion(self, action):
        """
        This method is the center piece for the function of the game.
        This method calls createQuestion in order to create a question then proceeds to ask it.
        It then handles the answer and calls scoreHandler to handle the score.
        
        In the normal function of the game, this method is called each step as the main game function.

        :type action: String
        :param action: The kind of question to be asked. 
            Acceptable parameters are normal, difficult, similar, normal-hint, difficult-hint or similar-hint

        :type return: list
        :param return: a list of returns of the form [reward, done, [question, answer]]
            reward is a string, representing the reward for the AI
            done is a boolean, informing if the game must stop for any reason.
            [question, answer] is a list of strings, which countains the question and the answer, returned for storage purposes
        """
        question_nature = action

        reward = 0
        # Here we declare two lists whose goal it is to fill multiple answer questions if no suitable data is to be found in the database
        noise_authors = ["Charles Darwin", "C.S. Lewis", "Marguerite Yourcenar", "J.D. Salinger"]
        noise_countries = ["France", "Germany", "Spain", "Canada", "China"]
        # x represents attempts at finding suitable noise data in the database
        x = 0
        
        try:
            # first we generate the question of the desired nature with createQuestions
            question = self.createQuestions(question_nature)

            # menial initiations, and actual printing of the question text
            # m_correct_answer is the number of the correct answer for the question
            m_correct_answer = None
            print(question[0])
            correct_answer = question[1]

            # This is the multiple answer block, where we choose noise data in order to create a random array of wrong answers
            wrong_answers = []
            if question_nature == "difficult" or question_nature == "difficult-hint":
                # this block choose 3 wrong answers that are not identical to the correct answer to serve as noise
                while(len(wrong_answers) < 3):
                    possible_wrong_answer = random.choice(list(self.system.masterdict.keys()))
                    if self.wrongAnswerChecker(correct_answer, possible_wrong_answer, wrong_answers):
                        wrong_answers.append(possible_wrong_answer)
            else:
                # this block choose 3 wrong answers that are not identical to the correct answer to serve as noise
                while(len(wrong_answers) < 3):
                    x+=1
                    if x < 20:
                        possible_wrong_answer = random.choice(list(self.system.masterdict.values()))[question[2]]
                    else:
                        if question[2] == "author":
                            possible_wrong_answer = random.choice(noise_authors)
                        if question[2] == "pubLoc":
                            possible_wrong_answer = random.choice(noise_countries)
                    if self.wrongAnswerChecker(correct_answer, possible_wrong_answer, wrong_answers):
                        wrong_answers.append(possible_wrong_answer)

            correct_answer_position = random.choice(range(1,5))
            for i in range(1,5):
                if i == correct_answer_position:
                    print(f"{i} : {correct_answer}")
                else:
                    print(f"{i} : {wrong_answers.pop()}")
            
            m_correct_answer = correct_answer_position


            # Answer processing block
            if question_nature != "completion":
                # User input
                answer = input("Enter the answer or the number of your answer: ")
                # answer = st.text_input("Enter the answer or the number of your answer: ")
                if answer == "exit()":
                    sys.exit(0)
                try:
                    # first we try to process the answer as an integer, if the answer is exactly equal to the answer or it's position in the multiple answers
                    # we call scoreHandler with True to process a correct answer
                    # elsewise, we call it with False to process an incorrect answer
                    ianswer = int(answer)
                    if ianswer == correct_answer or ianswer == m_correct_answer:
                        if question_nature != "extracted":
                            self.scoreHandler(True, question[2])
                            result = True
                            if "similar" in question_nature:
                                reward = 1
                            elif "difficult" in question_nature:
                                reward = 0.8
                            else:
                                reward = 0.5
                            if "hint" in question_nature:
                                reward += -0.1
                        # This else statement should never be reached, as extracted questions are not available
                        # It is kept for backwards compatibility and future expansion
                        else:
                            self.scoreHandler(True)
                            result = True
                    else:
                        if question_nature != "extracted":
                            self.scoreHandler(False, question[2])
                            result = False
                            if "similar" in question_nature:
                                reward = 0
                            elif "difficult" in question_nature:
                                reward = -0.25
                            else:
                                reward = -0.5
                            if "hint" in question_nature:
                                reward += -0.1
                        # This else statement should never be reached, as extracted questions are not available
                        # It is kept for backwards compatibility and future expansion
                        else:
                            self.scoreHandler(False)
                            result = False
                        print(f"The correct answer was {m_correct_answer} : {correct_answer}")
                except ValueError:
                    # if the user didn't give an int as an answer, we check if the answer is close enough to the answer (60% similar)
                    # to give some form of leeway in spelling mistakes
                    # we call scoreHandler with true if the answer is close enough
                    if not isinstance(correct_answer, int):
                        if fuzz.ratio(correct_answer, answer) > 60:
                            self.scoreHandler(True, question[2])
                            result = True
                            reward = 0.5
                        else:
                            self.scoreHandler(False, question[2])
                            result = False
                            reward = -0.5
                            print(f"The expected answer was : {correct_answer}")
                    else:
                        # This block should never be reached, but is kept just in case something catastrophic happens
                        self.scoreHandler(False, question[2])
                        print(f"The expected answer was : {correct_answer} and you gave me a string.")


        except:
            # This block handles exceptions and errors in the code and the answers of the user
            print("An error has occured. The program will generate a new question.")
            print(traceback.format_exc())
            return self.askQuestion(question_nature)

        print("================================")

        return reward, action, result
        # self.askQuestion()


    def scoreHandler(self,result,question_destination=None):
        """
        Method that controls and uses the score. It is called after a question has been answered.

        :type result: bool
        :param result: a boolean representation of sucess in the question that called this method

        :type question_destination: string or None
        :param question_destination: the type of question asked, used to control the player's metascore about a specific kind of question.
            None signifies a non-standard question has been asked.
        """
        if result:
            self.score += 1
            if question_destination:
                self.metascore[question_destination] += 1
                print(f"Your metascore for {question_destination} is now {self.metascore[question_destination]}")
            print(f"Congratulation, you won! Your score is now {self.score}")
        elif not result:
            self.score -= 1
            if question_destination:
                self.metascore[question_destination] -= 1
                print(f"Your metascore for {question_destination} is now {self.metascore[question_destination]}")
            print(f"Unfortunately, the answer was either wrong or too vague, your score is now {self.score}")


    def createQuestions(self, choice):
        """
        This method is private and should never be called directly.
        This method actually generates the parameters of the question, such as the question statement, the correct answer, etc.

        :type choice: string
        :param choice: the type of question that is to be created. 
            Acceptable parameters are normal, difficult, similar, normal-hint, difficult-hint or similar-hint

        :type return: tuple
        :param return: a tuple containing the generated data.
            The tuple is of the form (question, correct_answer, chosen_question_destination, self.system.masterdict[subject])
            Where question is the statement, correct_answer the expected answer, chosen_question_destination the type of metadata of the answer
            and finally self.system.masterdict[subject] is a dictionary describing the book about which we ask the question, returned for debugging purposes.
        """
        # choices = ["normal", "difficult", "hint", "difficult-hint", "similar"]
        # first we choose a subject at random. This subject will be overwritten by similar questions, but it's necessary for all other types of questions.
        subject = random.choice(list(self.system.masterdict.keys()))
        # A hint will be generated by hint questions, but it is established as none for all other questions.
        hint = None
        print(f"Question type: {choice}")

        # the similar and similar-hint questions are very similar, and only one will be commented
        if choice == "similar":
            # first we choose the similarity type of the book, 
            # meaning we choose in which way we want the book we are going to search is going to be to the previous one.
            similarity_type = random.choice(["genres", "author"])

            # search_list must be initiated with an element in it for correct function.
            search_list = ["Lorem Ipsum Dolor"]

            # here we create the reference string that will allow for searching the rdf graph, authors and genres are URI of the form dbr:name_name
            if similarity_type == "author" or similarity_type == "genres":
                reference_string = "dbr:"+self.system.masterdict[self._previous_question][similarity_type].replace(" ","_").replace("(", "\(").replace(")", "\)")
            # countries are string litterals
            elif similarity_type =="country":
                reference_string = self.system.masterdict[self._previous_question]["pubLoc"]
            # here we use the system method getBooks to fetch one new book from dbpedia that is similar in the way we chose from the last book.
            search_list[0] = reference_string
            similar = self.system.getBooks(search_list, search_type=similarity_type,max_returns=1)
            # we then use describeBook in order to insert it into the system.masterdict, and define it as our new subject
            self.system.describeBook(similar)
            subject = similar

            # the question is then handled exactly as a normal question.
            chosen_question_origin = "title"
            print(subject)
            possible_question_destination = [x for x in self.system.masterdict[subject].keys() if x != chosen_question_origin and self.system.masterdict[subject][x] != None and self.system.masterdict[subject][x] != "" and x != "_id"]

            if possible_question_destination:
                chosen_question_destination = random.choice(possible_question_destination)

            else:
                return self.createQuestions("similar")

        if choice == "similar-hint":

            # please refer to similar question for anotations.
            similarity_type = random.choice(["genres", "author", "country"])
            search_list = ["Lorem Ipsum Dolor"]
            if similarity_type == "author" or similarity_type == "genres":
                reference_string = "dbr:"+self.system.masterdict[self._previous_question][similarity_type].replace(" ","_").replace("(", "\(").replace(")", "\)")
            elif similarity_type =="country":
                reference_string = self.system.masterdict[self._previous_question]["pubLoc"]
            search_list[0] = reference_string
            similar = self.system.getBooks(search_list, search_type=similarity_type,max_returns=1)
            self.system.describeBook(similar)
            subject = similar

            chosen_question_origin = "title"
            possible_question_destination = [x for x in self.system.masterdict[subject].keys() if x != chosen_question_origin and self.system.masterdict[subject][x] != None and self.system.masterdict[subject][x] != "" and x != "_id"]

            if possible_question_destination:
                destination_sample = random.sample(possible_question_destination, 2)
                hint = self.system.masterdict[subject][destination_sample[0]]
                chosen_question_destination = destination_sample[1]
            else:
                return self.createQuestions("similar-hint")

        if choice == "normal-hint":
            chosen_question_origin = "title"
            possible_question_destination = [x for x in self.system.masterdict[subject].keys() if x != chosen_question_origin and self.system.masterdict[subject][x] != None and self.system.masterdict[subject][x] != "" and x != "_id"]

            if possible_question_destination:
                # here we generate the hint. Instead of picking one metadata to be asked about, we pick two.
                # one will be asked about, the other will be given as an extra piece of information.
                destination_sample = random.sample(possible_question_destination, 2)
                hint = self.system.masterdict[subject][destination_sample[0]]
                chosen_question_destination = destination_sample[1]

            else:
                # if the book contains nothing but it's title (which apparently is not that uncommon), pick again
                return self.createQuestions("hint")
        
        if choice == "normal":
            # normal questions are questions of the form "What is the 'secondary metadata' of 'title'?"
            # Thus, the origin (meaning the information given) is always title
            chosen_question_origin = "title"
            # the destination (what the user must find) can be any other data about the book, as long as this data is known by the database
            possible_question_destination = [x for x in self.system.masterdict[subject].keys() if x != chosen_question_origin and self.system.masterdict[subject][x] != None and self.system.masterdict[subject][x] != "" and x != "_id"]

            if possible_question_destination:
                chosen_question_destination = random.choice(possible_question_destination)

            else:
                # if the book contains nothing but it's title (which apparently is not that uncommon), pick again
                return self.createQuestions("normal")

        if choice == "difficult":
            # difficult questions behave in the opposite way of normal questions, they are of the form 'Among these 'title' which one is 'secondary metadata'?
            # thus, the destination (what the user must find) is always the title, and the origin can be any other metadata known.
            possible_questions_origin = [x for x in self.system.masterdict[subject].keys() if self.system.masterdict[subject][x] != None and x != "title" and self.system.masterdict[subject][x] != "" and x != "_id"]
            if possible_questions_origin:
                chosen_question_origin = random.choice(possible_questions_origin)
            else:
                return self.createQuestions("difficult")
            # and the destination is the title
            chosen_question_destination = "title"
        
        if choice == "difficult-hint":
            possible_questions_origin = [x for x in self.system.masterdict[subject].keys() if self.system.masterdict[subject][x] != None and x != "title" and self.system.masterdict[subject][x] != "" and x != "_id"]
            if possible_questions_origin:
                origin_sample = random.sample(possible_questions_origin, 2)
                hint = self.system.masterdict[subject][origin_sample[0]]
                chosen_question_origin = origin_sample[1]
            else:
                return self.createQuestions("difficult-hint")

            # and the destination is the title
            chosen_question_destination = "title"

        

        # we then make human readable questions from the question previously made with makeHumanQuestion
        if chosen_question_origin == "title":
            question = self.makeHumanQuestion(origin_subject=chosen_question_origin,origin=subject, destination_type=chosen_question_destination, hint=hint)
        if chosen_question_destination == "title":
            question = self.makeHumanQuestion(origin_subject=chosen_question_origin,origin=self.system.masterdict[subject][chosen_question_origin], destination_type=chosen_question_destination, hint=hint)

        # question = self.makeHumanQuestion(origin_subject=chosen_question_origin, origin=subject[chosen_question_origin], destination_type=chosen_question_destination, hint=hint)
        if choice == "difficult" or choice == "difficult-hint":
            correct_answer = subject
        else:
            correct_answer = self.system.masterdict[subject][chosen_question_destination]

        self._previous_question = subject
        
        # here we check if the answer to the question is missing in the database.
        # if it is, the question cannot be asked, and another question is generated.
        if correct_answer == None or correct_answer == "None":
            return self.createQuestions(choice)
        
        # we then retun a tuple which contains all the data we need
        return (question, correct_answer, chosen_question_destination, self.system.masterdict[subject])


    def makeHumanQuestion(self, origin_subject, origin, destination_type, hint=None):
        """
        Forms gramatical human-like questions from an origin and the destination subjects
        :type origin_subject: string
        :param origin_subject: the type of question

        :type origin: string
        :param origin: the data given to the player

        :type destination_type: string
        :param destination_type: that which the player must guess

        :param hint: A hint to be given to the player
        :type hint: None or string

        :type return: string
        """

        # this block handles easy questions
        if origin_subject == "title":
            if destination_type == "author":
                output_string = f"Who wrote {origin}?"
            if destination_type == "pubLoc":
                output_string = f"Where was first published {origin}?"
            if destination_type == "publisher":
                output_string = f"What is the name of the publishing firm that first published {origin}?"
            if destination_type == "pubDate":
                output_string = f"When was {origin} first published?"
            if destination_type == "genres":
                output_string = f"What genre does {origin} belong to?"
        
        # Difficult questions
        if origin_subject == "pubDate":
            if destination_type == "title":
                output_string = f"Among these books, which one was published in {origin}?"
            else:
                output_string = "Error in question processing (pubdate)"

        if origin_subject == "publisher":
            if destination_type == "title":
                output_string = f"Among these books, which one was published by {origin}?"
            else:
                output_string = "Error in question processing (publisher)"
        
        if origin_subject == "pubLoc":
            if destination_type == "title":
                output_string = f"Among these books, which one was published in {origin}?"
            else:
                output_string = "Error in question processing (pubLoc)"
        
        if origin_subject == "genres":
            if destination_type == "title":
                output_string = f"Among these books, which one belongs to the {origin} genre(s)?"
            else:
                output_string = "Error in question processing (genres)"

        if origin_subject == "author":
            if destination_type == "title":
                output_string = f"Among these books, which one was written by {origin}?"
            else:
                output_string = "Error in question processing (authors)"

        if hint:
            output_string += f" {hint}"

        return output_string

    def wrongAnswerChecker(self, correct_answer, wrong_answer, wrong_answers):
        """
        This utility checks wheater or not a wrong answer is suitable as noise for multiple answers questions.

        :param correct_answer: The correct answer to be checked against
        :type correct_answer: String

        :param wrong_answer: The wrong answer to be checked
        :type wrong_answer: String or None

        :param return: True if suitable, False otherwise
        :type return: Bool
        """
        USAlist = ["USA", "United States of America", "United States", "US"]
        UKlist = ["UK", "United Kingdom"]

        # Basic check, if both answers are identical, they are not suitable.
        if correct_answer == wrong_answer:
            return False


        # If the wrong answer was already used, the wrong answer is not acceptable.
        if wrong_answer in wrong_answers:
            return False
        
        for w in wrong_answers:
            if wrong_answer in w:
                return False

        # Checks for countries with common abreviations
        # Fist with wrong answers
        if wrong_answer in USAlist:
            if len(set(wrong_answers).intersection(set(USAlist))) > 0:
                return False
        if wrong_answer in UKlist:
            if len(set(wrong_answers).intersection(set(UKlist))) > 0:
                return False
        # Then with correct answers
        if correct_answer in USAlist:
            if wrong_answer in USAlist:
                return False
        if correct_answer in UKlist:
            if wrong_answer in UKlist:
                return False
        #If the wrong answer is corrupted or missing, it is not suitable.
        if wrong_answer == None or wrong_answer == "" or wrong_answer == "None":
            return False
        return True


def _main():
    """
    THIS MAIN EXISTS ONLY FOR DEBUGGING PURPOSES, THIS SCRIPT SHOULD NEVER BE CALLED ON IT'S OWN
    """
    # instanciate the class

    qa = QA()

    try:
        qa.askQuestion("similar")
        print(qa.system.masterdict)
    except KeyboardInterrupt:
        print("Program interrupted, shutting down...")
        sys.exit(0)
        


if __name__ == "__main__":
    _main()

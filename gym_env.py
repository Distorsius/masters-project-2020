"""
Masters Project 2020-2021 - Oberon
"""

import gym
from gym import spaces
from game import QA
import numpy

qa = QA()

class GameEnv(gym.Env):
    """
    This class serves as an interface between the algorithm and the game functions
    described in game and qasystem.
    It should not be instanciated manually, nor it's method be called by the user.
    """
    metadata = {'render.modes': ['human']}

    def __init__(self):
        super(GameEnv, self).__init__()    

        # here we declare the action space, meaning we define what options the AI can choose from at each step.
        # self.action_space = ["simple, difficult, simple-hint, difficult-hint, previous-correct-exact, previous-incorrect-exact, previous-correct-similar, previous-incorrect-similar"]  
        self.action_space = ["normal", "difficult", "normal-hint", "difficult-hint", "similar", "similar-hint"]

        # we then declare an empty observation space, 10 tuples of 3 elements each
        # this is to help maintain consistency between empty and full memory slots
        self.observation_space = [(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None),(None,None,None)]

        self.total_states = len(self.action_space)*10*2 + 1 
        self.current_step = 0


    def step(self, choice):
        """
        This function describes what the system as a whole should do each step,
        it is called every time step by the algorithm and it's returns serve
        as the feedback.

        :type action: string
        :param action: What action to be taken.

        :type return: tuple
        :param return: the results of the action, meaning the updated observation space and reward
        """

        # self._take_action(action)
        self.current_step += 1
        if type(choice) is str:
            action = choice
            result = None
        else:
            action, result = self.getObsFromNumber(choice)
        reward, action, result = qa.askQuestion(action)
        done = False
        if self.current_step >= 10:
            done = True
        self.nextObservation(reward, action, result)
        state = self.getStateNumber(action, result)
        return state, reward, done, {}


    def nextObservation(self, reward, action, result):
        """
        This function produces and stores 10 previous question-answer-rewards triplets
        """

        self.observation_space.pop(0)
        self.observation_space.append((reward,action,result))

        return self.observation_space

    def getStateNumber(self, action, result):
        """
        This function returns a unique number given an observation
        """
        # 10 superindices, with a perdiod of 12
        # so 0,12,24,36,etc.
        i = self.current_step * 12

        # 6 subindices, with a perdiod of 2
        j = self.action_space.index(action) * 2

        # 2 subsubindicies, with no period
        k = int(result)

        return i+j+k+1

    def getObsFromNumber(self, i):
        """
        This function takes a state number and deduces the observation
        """
        if not isinstance(i, str):
            action = self.action_space[i%6]
            result = bool(i%2)
        else:
            action = i
            result = None

        return action,result

    

    def reset(self):
        """
        This degenerated method only exists for consistency. It returns the initial state number, 0.
        """
        return 0



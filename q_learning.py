"""
Masters Project 2020-2021 - Oberon
"""


from gym_env import GameEnv
from game import QA
import random
import numpy as np
import pickle
import os
import argparse

ENV = GameEnv()

def main():

    #These lines handle the arguments parsing that allow for some form of user profiles, none of it is necessary for the program to function
    os.makedirs("users/tables", exist_ok=True)
    os.makedirs("users/databases",exist_ok=True)
    arg_parser = argparse.ArgumentParser(description="This script is designed to launch Oberon in it's q_learning form.")
    arg_parser.add_argument("-u", "--username", help="An unique ID.", default=None)
    arg_parser.add_argument("-n", "--new", help="Please specify if you are playing for the first time.", action="store_true")
    args = arg_parser.parse_args()
    user = args.username
    new_flag = args.new
    
    # This is the default profile of the game, if no user is defined.
    if not user:
        # First, if a qtable already exists, we load it with pickle. This serves as a long term memory.
        if os.path.exists("qtable.pickle"):
            with open("qtable.pickle", "rb") as inp:
                q_table = pickle.load(inp)
        else:
        # If no such table exists, we need to create an empty one. A zero matrix with dimension states*actions serves this purpose.
            q_table = np.zeros([ENV.total_states, len(ENV.action_space)])
    else:
        user_table_name = user+".pickle"
        user_table_path = os.path.normpath(os.path.join("users/tables/", user_table_name))
        user_db_name = user+".nt"
        user_db_path = os.path.normpath(os.path.join("users/databases/", user_db_name))
        if new_flag:
            if os.path.exists(user_table_path) or os.path.exists(user_db_path):
                raise Exception("This username already exists in the database, either uncheck the --new flag to continue playing of choose another name.")
            else:
                q_table = np.zeros([ENV.total_states, len(ENV.action_space)])
        else:
            try:
                with open(user_table_path, "rb") as inp:
                    q_table = pickle.load(inp)
            except FileNotFoundError:
                print("This profile does not exist, please check the --new flag if you intend on creating a new profile.")
                raise
            except:
                print("Something terrible has happened, please advise.")
                raise
            

    # Hyperparameters
    alpha = 0.3 # The learning rate, how much does new experience influence future choices. It is set absurdly high in order to allow the agent to adapt and learn quickly.
    gamma = 0.3 # The discount factor, that makes future experience to matter less than current ones. It is set somewhat low, because later learning is just as good as now learning.
    epsilon = 0.1 # The percent chance that the agent will ignore it's training and choose to explore the environment instead (by taking a random action)

    # We finalize initialisation by calling the reset method, storing it's return value as the current step.
    state = ENV.reset()
    reward = 0
    done = False
    
    # This is the loop in which the game actually plays.
    while not done:
        # This first if defines wheather or not the agent will follow it's learning
        # or try to explore the environment by picking an action at random
        if random.uniform(0, 1) < epsilon:
            action = random.choice(ENV.action_space) # Explore action space
            action = ENV.action_space.index(action)
        else:
            action = np.argmax(q_table[state]) # Exploit learned values
        # This line actually plays the game by calling the step action, and stores the return values.
        next_state, reward, done, info = ENV.step(action)
        # If the game is done, we break out of the loop now.
        if done:
            break

        # This is where Q_learning actually happens, first we take the previous value of the matrix
        old_value = q_table[state, action]
        next_max = np.max(q_table[next_state])
        
        # This is the bellman equation, that allows us to compute the new value of the action.
        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        q_table[state, action] = new_value

        
        state = next_state

    # Finally, we store the new and upgraded table in a file for later retrieval.
    if not user:
        with open("qtable.pickle", "wb") as out:
            pickle.dump(q_table, out)
    else:
        with open(user_table_path, "wb") as out:
            pickle.dump(q_table, out)
    if user:
        print(f"Thanks for playing, the AI's experience was successfully saved in the {user_table_path} file.")
    else:
        print("Thanks for playing, the AI's experience was successfully saved in the q_table.pickle file.")

if __name__ == "__main__":
    main()
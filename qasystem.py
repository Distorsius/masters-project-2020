"""
Masters Project 2020-2021 - Oberon
"""

import os, re, string, sys
import random
import traceback
from pprint import pprint
import json
from bson import json_util
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from SPARQLWrapper import SPARQLWrapper, JSON, XML
from SPARQLWrapper.SPARQLExceptions import QueryBadFormed, EndPointNotFound
import urllib,shutil
import rdflib
from rdflib import URIRef, Literal, BNode
from rdflib.namespace import FOAF, NamespaceManager, OWL, RDF
import pathlib



dirname = os.path.dirname(__file__)

class QAsystem:
    """
    This class contains the methods required for populating and controlling local databases.
    Under normal circumstances, this class should never be instanciated or called upon manually.
    However, the getBooks method can be used in order to obtain the ntriples files of any book.
    """

    def __init__(self):
        
        # we first call upon loadDbpediaOntology in order to load the dbpedia upper ontology and syntax
        # or the local version of the ontology should it already exist.
        self.loadDbpediaOntology()
        # we also declare the masterdict attribute as an empty dictionnary for later population.
        # this attribute serves as the working database for the game, and is populated by use of
        # the describeBook method.
        self.masterdict = dict()
            
        
        
    def getBooks(self, books=[], search_type = "title", max_returns = None, store = True, save_file = False):
        """
        This function uses sparql to recover .nt files that describe a book or list of books
        It can search using the title, litterary genre, year of publication or country of publication.

        :type book: list
        :param book: A list of book data to search for

        :type search_type: string
        :param search_type: the type of data used for searching, acceptable values are: title, genres, author, country

        :type max_return: int or None
        :param max_return: if specified, this will prevent the method from returning more than a certain number of books, even if more are found.

        :type store: boolean
        :param store: if true, the tmp.ntriple file that describes the books that were searched for 
            will be merged with the local database for permanent storage

        :type save_file: boolean
        :param store: if true, the tmp.ntriple file will not be deleted at the end of the method execution.

        :type return: None or string
        :param return: If max_return is 1, the method returns the title of the one book found. Otherwise, the method returns None.
        """
        # initiations, deleting the existing tmp.nt file if it exists
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        booksList = []
        if os.path.exists("tmp.nt"):
            os.remove("tmp.nt")
        
        # Sparql block
        for book in books:
            # For each book, create a sparql query
            booksQuery = ""
            booksQuery +="SELECT ?o ?lang WHERE{"
            # this series of ifs change the query in order to accomodate what type of data you might be searching for.
            if search_type == "title":
                booksQuery +=f'?o foaf:name "{book}"@en .'
            if search_type == "genres":
                # booksQuery +=f'?o dbo:literaryGenre "{book}" .'
                booksQuery +='{?o dbp:genre '+book+'} UNION {?o dbo:literaryGenre '+book+'}'
            if search_type == "author":
                booksQuery +=f'?o dbo:author {book} .'
            if search_type == "country":
                booksQuery +=f'?o dbp:country "{book}"@en .'
            booksQuery +="?o rdf:type bibo:Book ."
            booksQuery +="?o rdf:type dbo:Book ."
            booksQuery +="?o rdf:type dbo:WrittenWork ."
            booksQuery +="?o dbp:language ?lang ."
            booksQuery +="} LIMIT 25"
            sparql.setQuery(booksQuery)
            try:
                # Ask dbpedia with the sparql
                sparql.setReturnFormat(JSON)
                ret = sparql.query()
                booksDict = ret.convert()
                booksList.append(booksDict)
            except:
                raise
        
        # if max_returns is specified, we use a sample of the returned booksList of the specified size as a return
        # instead of all the books found
        if max_returns:
            try:
                booksList = random.sample(booksList, max_returns)
            except ValueError:
                pass

        for item in booksList:
            for result in item["results"]["bindings"]:
                for key in result.keys():
                    if key == "o":
                        # here we create the link for the ntriple file instead of it's HTML counterpart
                        # that is returned by default by dpedia.
                        url = result[key]["value"]
                        url = url.replace("resource", "data")
                        url += ".ntriples"
                        try:
                            with urllib.request.urlopen(url) as response, open("tmp.nt", 'a') as out:
                                lines = response.readlines()
                                for line in lines:
                                    # here we correct a few encoding errors, namely, we explicitely replace
                                    # the string "\u0027" by "'". Afterward, we exclude all lines that contains
                                    # a "\" from the ntriple files, as it means it contains a decoding error we
                                    # cannot deal with.
                                    w_line = line.decode()
                                    w_line = re.sub("\\\\u0027", "'", w_line)
                                    if not re.search("\\\\", w_line):
                                        if re.search("@", w_line):
                                            if re.search("@en", w_line):
                                                out.write(w_line)
                                            else:
                                                pass
                                        else:
                                            out.write(w_line)
                        except urllib.error.HTTPError as exception:
                            print(exception.code)
                            print(exception.msg)
                            print(exception.hdrs)
                            print(exception.fp)
                        except UnicodeEncodeError:
                            pass
                            
        
        # in this block, we store the return value of the function to title if max_returns ==1
        if max_returns == 1:
            g = rdflib.Graph()
            g.parse("tmp.nt", format="nt")
            for s,p,o in g:
                if str(p) == "http://xmlns.com/foaf/0.1/name":
                    title = str(o)
        # here, if store is true, we merge the tmp.nt file that describes the books obtained in sparql query
        # with the local database for permanent storage.
        if store:
            self.dbpedia_graph.parse("tmp.nt", format="nt")

            self.dbpedia_graph.serialize("dbpedia_graph.nt", format="nt")

        # if save_file is false, we remove the tmp file
        if not save_file:
            os.remove("tmp.nt")
        
        # finally, if max_returns is 1 we return the title, else we return None
        if max_returns == 1:
            return title
        return None


    def describeBook(self, bookname):
        """
        This method uses SPAQRL queries to describe an individual book
        and returns a dictionary containing the description
        then stores the dictionary in a master dictionary, whose keys are
        the subjects

        The subdictionary is of the form
        keys = predicates
        values = objects

        :type bookname: string
        :param bookname: the title of the book to be described

        :type return: None
        """
        subdict = dict()
        # here we create a sparql query to use on the local database in order to obtain the triples of interest to us,
        # specifically the ones that concern the book we search for.
        query = ""
        query+="SELECT DISTINCT ?title ?pubDate ?pubLoc ?lang ?author ?publisher"
        query+="(GROUP_CONCAT(?genre;SEPARATOR=',') as ?genres)"
        query+="WHERE{"
        query+=f'?o foaf:name "{bookname}"@en .'
        query+="?o foaf:name ?title ."
        query+="OPTIONAL{?o dbo:literaryGenre ?genre} ."
        query+="OPTIONAL{?o dbp:genre ?genre} ."
        query+="?o dbp:language ?lang ."
        query+="OPTIONAL{?o dbo:author ?author} ."
        query+="OPTIONAL{?o dbp:country ?pubLoc} ."
        query+="OPTIONAL{?o dbp:published ?pubDate} ."
        query+="OPTIONAL{?o dbp:pubDate ?pubDate} ."
        query+="OPTIONAL{?o dbo:releaseDate ?pubDate} ."
        query+="OPTIONAL{?o dbo:publisher ?publisher} ."
        query+="{ ?o a dbo:Book } UNION { ?o a bibo:Book } UNION { ?o a dbo:WrittenWork }"
        query+="}"

        results = self.dbpedia_graph.query(query)

        # we then convert those results into a dictionnary and store it in the masterdict attribute as a subdict.
        for result in results:
            subdict["author"] = str(result.author).split("/")[-1].replace("_", " ")
            subdict["publisher"] = str(result.publisher).split("/")[-1].replace("_", " ")
            subdict["pubLoc"] = str(result.pubLoc).split("/")[-1].replace("_", " ")
            subdict["pubDate"] = str(result.pubDate)
            subdict["genres"] = str(result.genres).split("/")[-1].replace("_", " ")
            self.masterdict[str(result.title)] = subdict


    def loadDbpediaOntology(self):
        """
        if a local ontology is not already present,
        this method loads the base syntax ontology of dbpedia.
        otherwise, it loads the local ontology.

        :type return: None
        """
        # here we declare the graph, and create it's namespace to match the namespace from dbpedia
        self.dbpedia_graph = rdflib.Graph()
        self.dbpedia_graph.namespace_manager.bind('dbr', URIRef("http://dbpedia.org/resource/"))
        self.dbpedia_graph.namespace_manager.bind('dbp', URIRef("http://dbpedia.org/property/"))
        self.dbpedia_graph.namespace_manager.bind('dbo', URIRef("http://dbpedia.org/ontology/"))
        self.dbpedia_graph.namespace_manager.bind('yago', URIRef("http://dbpedia.org/class/yago/"))
        self.dbpedia_graph.namespace_manager.bind('yagor', URIRef("http://yago-knowledge.org/resource/"))
        self.dbpedia_graph.namespace_manager.bind('schema', URIRef("http://schema.org/"))
        self.dbpedia_graph.namespace_manager.bind('bibo', URIRef("http://purl.org/ontology/bibo/"))
        self.dbpedia_graph.namespace_manager.bind('rdf', RDF)
        self.dbpedia_graph.namespace_manager.bind('owl', OWL)
        self.dbpedia_graph.namespace_manager.bind('foaf', FOAF)

        # if a local version of the database does not exist, we get the upper ontology from dbpedia
        if not os.path.exists("dbpedia_graph.nt"):
            try:
                self.dbpedia_graph.parse("ontology_type=parsed.owl")
            except FileNotFoundError:
                self.dbpedia_graph.parse("http://archivo.dbpedia.org/download?o=http%3A//dbpedia.org/ontology/&f=owl", format="xml")
                self.getBooks(["Dune", "Pride and Prejudice", "A Brief History of Time", "The Fellowship of the Ring", "Alice's Adventures in Wonderland"])     
            except:
                print("Error in loading dbpedia upper ontology, please check your internet connexion or download ontology shema @http://archivo.dbpedia.org/download?o=http%3A//dbpedia.org/ontology/&f=owl and place into the installation folder.")
        # otherwise, we load the local version.
        else:
            self.dbpedia_graph.parse("dbpedia_graph.nt", format="nt")

    def obtainAllTitles(self):
        """
        This utility method parses the local graph in order to obtain all existing book titles
        It then returns a list containing them all

        :param return: A list of titles
        :type return: list (strings)
        """

        query = """
                select distinct ?title where{
                    ?o foaf:name ?title
                }
                """
        results = self.dbpedia_graph.query(query)

        returnList = list()
        for row in results:
            returnList.append(str(row.title))
            
        return returnList

def _main():
    """
    THIS MAIN EXISTS ONLY FOR DEBUGGING PURPOSES, THIS SCRIPT SHOULD NEVER BE CALLED ON IT'S OWN
    """
    # instanciate the class
    qa = QAsystem()
    # qa.getBooks(["dbr:Stephen_Hawking"], search_type="author", store=False, save_file=True)
    # qa.getBooks(["Dune", "Pride and Prejudice", "A Brief History of Time", "The Fellowship of the Ring", "Alice's Adventures in Wonderland"])
    # qa.describeBook("Dune")
    qa.obtainAllTitles()


if __name__ == "__main__":
    _main()

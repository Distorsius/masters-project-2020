from gym_env import GameEnv
from game import QA
import random
import numpy as np

ENV = GameEnv()

def main():
    count = 0
    done = False
    while not done:
        action = random.choice(["normal", "difficult", "normal-hint", "difficult-hint"])
        # action = random.choice(["normal", "difficult", "normal-hint", "difficult-hint", "similar", "similar-hint"])
        if count == 0:
            action = "normal"
        next_state, reward, done, info = ENV.step(action)
        count += 1
        if done:
            break
    print("Thanks for playing!")

if __name__ == "__main__":
    main()